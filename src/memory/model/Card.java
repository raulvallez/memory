/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memory.model;

import java.util.Map;
import javafx.scene.image.ImageView;

/**
 *
 * @author rvallez
 */
public class Card {
    
    private ImageView image;
    private boolean isFound;

    public Card(ImageView image, boolean isFound) {
        this.image = image;
        this.isFound = isFound;
    }

    public ImageView getImage() {
        return image;
    }

    public boolean isIsFound() {
        return isFound;
    }

    public void setIsFound(boolean isFound) {
        this.isFound = isFound;
    }

}
