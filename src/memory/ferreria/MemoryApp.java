package memory.ferreria;

import buscamines.MemoryContract.MemoryModel;
import buscamines.MemoryContract.MemoryPresenter;
import buscamines.MemoryContract.MemoryView;
import buscamines.model.MemoryModelImpl;
import buscamines.control.MemoryPresenterImpl;
import buscamines.vista.MemoryViewImpl;
import java.io.IOException;
import javafx.application.Application;
import javafx.stage.Stage;

public class MemoryApp extends Application {

    @Override
    public void start(Stage stage) throws IOException {

        MemoryPresenter p;
        MemoryModel m;
        MemoryView v;

        p = new MemoryPresenterImpl();
        m = new MemoryModelImpl();

        p.setModel(m);
        v = new MemoryViewImpl(stage, p);
        p.setView(v);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
