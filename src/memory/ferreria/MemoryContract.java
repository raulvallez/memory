package memory.ferreria;

import buscamines.BuscaminesContract.BuscaminesModel;
import buscamines.model.BuscaminesModelImpl.Difficult;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javafx.scene.image.ImageView;
import memory.model.Card;
import resources.Temes;

public interface MemoryContract {

    interface MemoryGame {

        // methods that can be called by presenter
        void start(Temes tema);
        
        ImageView discoverCard(int pos);

        boolean isOver();    
    }

    //the model contract     
    interface MemoryModel extends MemoryGame {

        void setPresenter(MemoryPresenter p);

        boolean addListener(MemoryModelListener listener);

        boolean removeListener(MemoryModelListener listener);
    }

    //the presenter contract     
    interface MemoryPresenter extends MemoryModelListener{

        void setModel(MemoryModel m);

        void setView(MemoryView v);

        // methods called by view 
        void toUncover(int pos);

        Set<Integer> configSizes();

        void toRestart(int size, Difficult d);
        
        

    }

    //the view contract     
    interface MemoryView {

        void setPresenter(MemoryPresenter p);

        // methods called by presenter
        void uncovered(Map<Integer, Integer> boxesToUnCovered);

        void overGame(List<Integer> posMines);

        void win();
    }

    interface MemoryModelListener {
        //when the game is over the listeners are notified. 

        void overEvent(List<Integer> posMines);

        void winEvent();
        
        boolean cardIsFoundEvent();
    }
}
